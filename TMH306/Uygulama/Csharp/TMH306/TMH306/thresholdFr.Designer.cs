﻿namespace TMH306
{
    partial class thresholdFr
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.dosyaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.açToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.thresholdYöntemleriToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.önTanımlıelleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.önTanımlıToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.otsuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.islemBox = new System.Windows.Forms.PictureBox();
            this.kaynakBox = new System.Windows.Forms.PictureBox();
            this.bradleyLocalThresholdToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.iterativeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.islemBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kaynakBox)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dosyaToolStripMenuItem,
            this.thresholdYöntemleriToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1336, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // dosyaToolStripMenuItem
            // 
            this.dosyaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.açToolStripMenuItem});
            this.dosyaToolStripMenuItem.Name = "dosyaToolStripMenuItem";
            this.dosyaToolStripMenuItem.Size = new System.Drawing.Size(51, 20);
            this.dosyaToolStripMenuItem.Text = "Dosya";
            // 
            // açToolStripMenuItem
            // 
            this.açToolStripMenuItem.Name = "açToolStripMenuItem";
            this.açToolStripMenuItem.Size = new System.Drawing.Size(88, 22);
            this.açToolStripMenuItem.Text = "Aç";
            this.açToolStripMenuItem.Click += new System.EventHandler(this.açToolStripMenuItem_Click);
            // 
            // thresholdYöntemleriToolStripMenuItem
            // 
            this.thresholdYöntemleriToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.önTanımlıelleToolStripMenuItem,
            this.önTanımlıToolStripMenuItem,
            this.otsuToolStripMenuItem,
            this.bradleyLocalThresholdToolStripMenuItem,
            this.iterativeToolStripMenuItem});
            this.thresholdYöntemleriToolStripMenuItem.Name = "thresholdYöntemleriToolStripMenuItem";
            this.thresholdYöntemleriToolStripMenuItem.Size = new System.Drawing.Size(131, 20);
            this.thresholdYöntemleriToolStripMenuItem.Text = "Threshold Yöntemleri";
            // 
            // önTanımlıelleToolStripMenuItem
            // 
            this.önTanımlıelleToolStripMenuItem.Name = "önTanımlıelleToolStripMenuItem";
            this.önTanımlıelleToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.önTanımlıelleToolStripMenuItem.Text = "Ön tanımlı (elle)";
            this.önTanımlıelleToolStripMenuItem.Click += new System.EventHandler(this.önTanımlıelleToolStripMenuItem_Click);
            // 
            // önTanımlıToolStripMenuItem
            // 
            this.önTanımlıToolStripMenuItem.Name = "önTanımlıToolStripMenuItem";
            this.önTanımlıToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.önTanımlıToolStripMenuItem.Text = "Ön tanımlı";
            this.önTanımlıToolStripMenuItem.Click += new System.EventHandler(this.önTanımlıToolStripMenuItem_Click);
            // 
            // otsuToolStripMenuItem
            // 
            this.otsuToolStripMenuItem.Name = "otsuToolStripMenuItem";
            this.otsuToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.otsuToolStripMenuItem.Text = "Otsu";
            this.otsuToolStripMenuItem.Click += new System.EventHandler(this.otsuToolStripMenuItem_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // islemBox
            // 
            this.islemBox.Location = new System.Drawing.Point(748, 66);
            this.islemBox.Margin = new System.Windows.Forms.Padding(2);
            this.islemBox.Name = "islemBox";
            this.islemBox.Size = new System.Drawing.Size(460, 480);
            this.islemBox.TabIndex = 9;
            this.islemBox.TabStop = false;
            // 
            // kaynakBox
            // 
            this.kaynakBox.Location = new System.Drawing.Point(46, 66);
            this.kaynakBox.Margin = new System.Windows.Forms.Padding(2);
            this.kaynakBox.Name = "kaynakBox";
            this.kaynakBox.Size = new System.Drawing.Size(460, 480);
            this.kaynakBox.TabIndex = 8;
            this.kaynakBox.TabStop = false;
            // 
            // bradleyLocalThresholdToolStripMenuItem
            // 
            this.bradleyLocalThresholdToolStripMenuItem.Name = "bradleyLocalThresholdToolStripMenuItem";
            this.bradleyLocalThresholdToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.bradleyLocalThresholdToolStripMenuItem.Text = "Bradley local threshold";
            this.bradleyLocalThresholdToolStripMenuItem.Click += new System.EventHandler(this.bradleyLocalThresholdToolStripMenuItem_Click);
            // 
            // iterativeToolStripMenuItem
            // 
            this.iterativeToolStripMenuItem.Name = "iterativeToolStripMenuItem";
            this.iterativeToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.iterativeToolStripMenuItem.Text = "Iterative";
            this.iterativeToolStripMenuItem.Click += new System.EventHandler(this.iterativeToolStripMenuItem_Click);
            // 
            // thresholdFr
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1336, 666);
            this.Controls.Add(this.islemBox);
            this.Controls.Add(this.kaynakBox);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "thresholdFr";
            this.Text = "thresholdFr";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.islemBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kaynakBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem dosyaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem açToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem thresholdYöntemleriToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem önTanımlıelleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem önTanımlıToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.PictureBox islemBox;
        private System.Windows.Forms.PictureBox kaynakBox;
        private System.Windows.Forms.ToolStripMenuItem otsuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bradleyLocalThresholdToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem iterativeToolStripMenuItem;
    }
}