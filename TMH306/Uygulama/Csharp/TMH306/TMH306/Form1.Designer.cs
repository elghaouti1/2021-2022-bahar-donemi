﻿
namespace TMH306
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.işlemlerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pikselAlToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.invertToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.parlaklıkToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.griYöntemleriToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.thresholdYöntemleriToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lowHighPassToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.segmentasyonToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.işlemlerToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(289, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // işlemlerToolStripMenuItem
            // 
            this.işlemlerToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pikselAlToolStripMenuItem,
            this.invertToolStripMenuItem,
            this.parlaklıkToolStripMenuItem,
            this.griYöntemleriToolStripMenuItem,
            this.thresholdYöntemleriToolStripMenuItem,
            this.lowHighPassToolStripMenuItem,
            this.segmentasyonToolStripMenuItem});
            this.işlemlerToolStripMenuItem.Name = "işlemlerToolStripMenuItem";
            this.işlemlerToolStripMenuItem.Size = new System.Drawing.Size(60, 20);
            this.işlemlerToolStripMenuItem.Text = "İşlemler";
            // 
            // pikselAlToolStripMenuItem
            // 
            this.pikselAlToolStripMenuItem.Name = "pikselAlToolStripMenuItem";
            this.pikselAlToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.pikselAlToolStripMenuItem.Text = "Piksel Al";
            this.pikselAlToolStripMenuItem.Click += new System.EventHandler(this.pikselAlToolStripMenuItem_Click);
            // 
            // invertToolStripMenuItem
            // 
            this.invertToolStripMenuItem.Name = "invertToolStripMenuItem";
            this.invertToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.invertToolStripMenuItem.Text = "Invert";
            this.invertToolStripMenuItem.Click += new System.EventHandler(this.invertToolStripMenuItem_Click);
            // 
            // parlaklıkToolStripMenuItem
            // 
            this.parlaklıkToolStripMenuItem.Name = "parlaklıkToolStripMenuItem";
            this.parlaklıkToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.parlaklıkToolStripMenuItem.Text = "Parlaklık";
            this.parlaklıkToolStripMenuItem.Click += new System.EventHandler(this.parlaklıkToolStripMenuItem_Click);
            // 
            // griYöntemleriToolStripMenuItem
            // 
            this.griYöntemleriToolStripMenuItem.Name = "griYöntemleriToolStripMenuItem";
            this.griYöntemleriToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.griYöntemleriToolStripMenuItem.Text = "Gri yöntemleri";
            this.griYöntemleriToolStripMenuItem.Click += new System.EventHandler(this.griYöntemleriToolStripMenuItem_Click);
            // 
            // thresholdYöntemleriToolStripMenuItem
            // 
            this.thresholdYöntemleriToolStripMenuItem.Name = "thresholdYöntemleriToolStripMenuItem";
            this.thresholdYöntemleriToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.thresholdYöntemleriToolStripMenuItem.Text = "Threshold yöntemleri";
            this.thresholdYöntemleriToolStripMenuItem.Click += new System.EventHandler(this.thresholdYöntemleriToolStripMenuItem_Click);
            // 
            // lowHighPassToolStripMenuItem
            // 
            this.lowHighPassToolStripMenuItem.Name = "lowHighPassToolStripMenuItem";
            this.lowHighPassToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.lowHighPassToolStripMenuItem.Text = "Low High Pass";
            this.lowHighPassToolStripMenuItem.Click += new System.EventHandler(this.lowHighPassToolStripMenuItem_Click);
            // 
            // segmentasyonToolStripMenuItem
            // 
            this.segmentasyonToolStripMenuItem.Name = "segmentasyonToolStripMenuItem";
            this.segmentasyonToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.segmentasyonToolStripMenuItem.Text = "Segmentasyon";
            this.segmentasyonToolStripMenuItem.Click += new System.EventHandler(this.segmentasyonToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(289, 213);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Form1";
            this.Text = "Ana Form";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem işlemlerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pikselAlToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem invertToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem parlaklıkToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem griYöntemleriToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem thresholdYöntemleriToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lowHighPassToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem segmentasyonToolStripMenuItem;
    }
}

