﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AForge;
using AForge.Imaging.Filters;

namespace TMH306
{
    public partial class thresholdFr : Form
    {
        Bitmap kaynak, islem;
        public thresholdFr()
        {
            InitializeComponent();
        }

        private void önTanımlıelleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int gen = kaynak.Width;
            int yuk = kaynak.Height;
            islem = new Bitmap(gen, yuk);
            int tValue = 100;

            for (int y = 0; y < yuk; y++)
            {
                for (int x = 0; x < gen; x++)
                {
                    Color piksel = kaynak.GetPixel(x, y);
                    int red = piksel.R;
                    int green = piksel.G;
                    int blue = piksel.B;

                    int griDeger = (red + green + blue) / 3;
                    if (griDeger < tValue) griDeger = 0; else griDeger = 255;
                    Color yPiksel = Color.FromArgb(griDeger, griDeger, griDeger);
                    islem.SetPixel(x, y, yPiksel);
                }
            }

            islemBox.Image = islem;
        }

        private void önTanımlıToolStripMenuItem_Click(object sender, EventArgs e)
        {
            islem = new GrayscaleBT709().Apply(kaynak);
            islem = new Threshold(100).Apply(islem);
            //islem=new Invert().Apply(islem);
            islemBox.Image = islem;
        }

        private void otsuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            islem = new GrayscaleBT709().Apply(kaynak);
            islem = new OtsuThreshold().Apply(islem);
            islemBox.Image = islem;
        }

        private void bradleyLocalThresholdToolStripMenuItem_Click(object sender, EventArgs e)
        {
            islem = new GrayscaleBT709().Apply(kaynak);
            islem = new BradleyLocalThresholding().Apply(islem);
            islemBox.Image = islem;
        }

        private void iterativeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            islem = new GrayscaleBT709().Apply(kaynak);
            islem = new IterativeThreshold(2,100).Apply(islem);
            islemBox.Image = islem;
        }

        private void açToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult result = openFileDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                kaynak = new Bitmap(openFileDialog1.FileName);
                kaynakBox.Image = kaynak;
            }
        }
    }
}
